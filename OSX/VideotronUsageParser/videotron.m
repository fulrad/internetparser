//
//  videotron.m
//  StatusMenuApp
//
//  Created by Francis Valois on 11-02-25.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "videotron.h"
#import <RegexKit/RegexKit.h>

@implementation videotron

@synthesize connectionURL;
@synthesize infoURL;
@synthesize username;
@synthesize password;
@synthesize gbMax;
@synthesize gbDone;
@synthesize gbUpload;
@synthesize gbDownload;
@synthesize joursRestants;


-(id) init
{
	if ( self = [super init] )
    {
		[self setConnectionURL:@"https://www.videotron.com/client/user-management/affaires/secur/Login.do"];
		[self setInfoURL:@"https://www.videotron.com/client/affaires/secur/CIUserSecurise.do"];
		[self setUsername:@"slvnperron"];
		[self setPassword:@"b1idyq54"];
		[self setGbMax:500];
    }
    return self;
}
-(void) connection
{	
	//Création du post
	NSString *post = [NSString stringWithFormat:@"dispatch=login&appId=EC&domain=domainType.business&cookieEnabled=true&codeUtil=%@&motDePasse=%@", self.username, self.password]; 
	NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
	NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];


	//Création de la requete HTTP de connection
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
	[request setURL:[NSURL URLWithString:self.connectionURL]];
	[request setHTTPMethod:@"POST"];
	[request setValue:postLength forHTTPHeaderField:@"Content-Length"];
	[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
	[request setValue:@"keep-alive" forHTTPHeaderField:@"Connection"];
	[request setValue:@"https://www.videotron.com/client/user-management/secur/Logout.do?dispatch=logout&lang=fr" forHTTPHeaderField:@"Referer"];
	[request setValue:@"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; fr; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13" forHTTPHeaderField:@"User-Agent"];
	[request setHTTPBody:postData]; 
	
	//Execution de la requete HTTP de connection
	NSHTTPURLResponse *response=nil;
	NSData *data;  
	data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:NULL];

	[request release];
}
-(NSString*) getValue
{

	NSMutableURLRequest *request2 = [[NSMutableURLRequest alloc] init];
	
	[request2 setURL:[NSURL URLWithString:self.infoURL]];
	[request2 setHTTPMethod:@"GET"];
	[request2 setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
	[request2 setValue:@"keep-alive" forHTTPHeaderField:@"Connection"];
	[request2 setValue:@"https://www.videotron.com/client/user-management/secur/Logout.do?dispatch=logout&lang=fr" forHTTPHeaderField:@"Referer"];
	[request2 setValue:@"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; fr; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13" forHTTPHeaderField:@"User-Agent"]; 
	
	//Exécution de la requete HTTP de parsing
	NSURLResponse *response2=nil;
	NSData *data2;  
	data2 = [NSURLConnection sendSynchronousRequest:request2 returningResponse:&response2 error:NULL];
	NSString* content2 = [[NSString alloc]  initWithBytes:[data2 bytes]
												   length:[data2 length] encoding: NSASCIIStringEncoding];
	
	
	[request2 release];
	
	return [content2 autorelease];
}

-(int) getInfo
{
	for (int i = 0; i < 3; i++) {

		[self connection];
		
		NSString* content2 = [self getValue];
		
		//Regex pour allez cherche la bande passante et le nombre de jours restants
		NSString *uploadDone = NULL;
		NSString *downloadDone = NULL;
		NSString *totalDownload = NULL;
		NSString *endOfContract = NULL;
		
		//Regex pour l'upload, le download et le total
		NSString *regexString   = @"<span class=\"quantities\">([0-9]+,*[0-9]*)&nbsp;Go</span>";
		RKEnumerator *matchEnumerator = [content2 matchEnumeratorWithRegex:regexString];
		[matchEnumerator nextRanges];
		downloadDone = [matchEnumerator stringWithReferenceString:@"${1}"];
		[matchEnumerator nextRanges];
		uploadDone = [matchEnumerator stringWithReferenceString:@"${1}"];
		[matchEnumerator nextRanges];
		totalDownload = [matchEnumerator stringWithReferenceString:@"${1}"];
		
		//Regex pour la date de fin du mois
		[content2 getCapturesWithRegexAndReferences:@"<h3>Consommation du  [0-9]* [a-z]* au ([0-9]*)",
													@"$1", &endOfContract,
													nil];
		int endDate = [endOfContract intValue];
		
		//Get date and number of days in the month
		NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
		NSDateComponents *dateComponents = [gregorian components:NSDayCalendarUnit fromDate:[NSDate date]];
		NSInteger todayDate = [dateComponents day];
		NSRange rng = [gregorian rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:[NSDate date]];
		NSUInteger numberOfDaysInMonth = rng.length;
		[gregorian release];
		
		//Get number of days left
		if (todayDate > endDate) {
			self.joursRestants = numberOfDaysInMonth - todayDate + endDate;
		}
		else if (endDate > todayDate){
			self.joursRestants =  endDate - todayDate;
		}
		else{
			self.joursRestants = 0;
		}
		
		//Transformation des , en . pour un parsing en float
		NSString* downloadDoneReal = [downloadDone stringByReplacingOccurrencesOfString:@","
																   withString:@"."];
		NSString* uploadDoneReal = [uploadDone stringByReplacingOccurrencesOfString:@","
																			 withString:@"."];
		NSString* totalDownloadReal = [totalDownload stringByReplacingOccurrencesOfString:@","
																		  withString:@"."];
		//Modification des valeurs de la classe
		self.gbDone = [totalDownloadReal floatValue];
		self.gbUpload = [uploadDoneReal floatValue];
		self.gbDownload = [downloadDoneReal floatValue];
		
		if (self.gbMax != 0)
			break;
		
	}
	return 1;
}
@end
