//
//  StatusMenuAppAppDelegate.m
//  StatusMenuApp
//
//  Created by Francis Valois on 11-02-25.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "StatusMenuAppAppDelegate.h"
#import "videotron.h"
#import "Controller.h"
@implementation StatusMenuAppAppDelegate

@synthesize window;

- (void) applicationWillFinishLaunching:(NSNotification *)aNotification {
	[self updateValue:NULL];
}

-(void)awakeFromNib{
	//Liaison du menu au systemMenuBar
	statusItem = [[[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength] retain];
	[statusItem setMenu:statusMenu];
	[statusItem setTitle:@""];
	
	//Création de l'icone
	NSString *dir = [NSString stringWithFormat:@""];
	NSString *file = [NSString stringWithFormat:@"icon"]; 
	NSString *path = [[NSBundle mainBundle] pathForResource:file ofType:@"png" inDirectory:dir];
	NSImage* image = [[NSImage alloc] initByReferencingFile:path];
	NSSize* size = malloc(sizeof(NSSize));
	size->width = 18;
	size->height = 18;
	[image setSize:*size];
	free(size);
	[statusItem setImage:image];
	[image release];
	[statusItem setHighlightMode:YES];
	
	//Création du timer sur la fonction de fecthing aux 16 minutes
	[NSTimer scheduledTimerWithTimeInterval:1000
									   target:self
									 selector:@selector(timerUpdate:)
									 userInfo:nil
									  repeats:YES];
}
- (IBAction)updateValue:(id)sender {
	[NSThread detachNewThreadSelector: @selector(updateValueCalc)
							 toTarget: self withObject: nil];
	
}

- (IBAction) quitApp:(id)sender
{
	[NSApp terminate:self];
}
- (void)timerUpdate:(NSTimer*)theTimer {
	[self updateValueCalc];
}

- (void) updateMenu:(NSArray*) Array {
	[affichageUpload setAttributedTitle:[Array objectAtIndex:1]];
	[affichageDownload setAttributedTitle:[Array objectAtIndex:0]];
	[affichageDownloadTotal setAttributedTitle:[Array objectAtIndex:2]];
	[affichageJour setAttributedTitle:[Array objectAtIndex:3]];
}

- (void) updateValueCalc
{
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
	
	//Creation de l'object vidotron
	videotron* info = [[videotron alloc] init];
	[info getInfo];
	
	//Creation du string downloadTotal avec la bonne taille et la bonne couleur
	NSString* utilisation = [NSString stringWithFormat:@"Utilisation Web : %.2f Go / %.2f Go", info.gbDone, info.gbMax];
	NSMutableAttributedString* downloadTotal = [[NSMutableAttributedString alloc] initWithString:utilisation];
	[downloadTotal addAttribute:NSForegroundColorAttributeName value:[NSColor grayColor] range:NSMakeRange(0,[downloadTotal length])];
	if((info.gbDone/info.gbMax) >= 0.70 && (info.gbDone/info.gbMax) < 0.70)
	{
		[downloadTotal addAttribute:NSForegroundColorAttributeName value:[NSColor yellowColor] range:NSMakeRange(0,[downloadTotal length])];
	}
	if((info.gbDone/info.gbMax) >= 0.95)
	{
		[downloadTotal addAttribute:NSForegroundColorAttributeName value:[NSColor redColor] range:NSMakeRange(0,[downloadTotal length])];
	}
	[downloadTotal addAttribute:NSFontAttributeName value:[NSFont menuBarFontOfSize:0] range:NSMakeRange(0,[downloadTotal length])];
	
	//Création du string de jours restants conjugé avec la bonne taille et couleur
	NSMutableString* joursRestants = [NSMutableString stringWithFormat:@""];
	if(info.joursRestants > 1)
	{
		NSString* joursString = [NSString stringWithFormat:@"Jours restants : %d", info.joursRestants];
		[joursRestants setString:joursString];
	}
	else
	{
		NSString* joursString = [NSString stringWithFormat:@"Jour restant : %d", info.joursRestants];
		[joursRestants setString:joursString];
	}
	NSMutableAttributedString* jours = [[NSMutableAttributedString alloc] initWithString:joursRestants];
	[jours addAttribute:NSForegroundColorAttributeName value:[NSColor grayColor] range:NSMakeRange(0,[jours length])];
	[jours addAttribute:NSFontAttributeName value:[NSFont menuBarFontOfSize:0] range:NSMakeRange(0,[jours length])];
	
	//Download String
	NSMutableString* download = [NSString stringWithFormat:@"Download : %.2f Go", info.gbDownload];
	NSMutableAttributedString* downloadString = [[NSMutableAttributedString alloc] initWithString:download];
	[downloadString addAttribute:NSForegroundColorAttributeName value:[NSColor grayColor] range:NSMakeRange(0,[downloadString length])];
	[downloadString addAttribute:NSFontAttributeName value:[NSFont menuBarFontOfSize:0] range:NSMakeRange(0,[downloadString length])];
	
	//Upload String
	NSMutableString* upload = [NSString stringWithFormat:@"Upload : %.2f Go", info.gbUpload];
	NSMutableAttributedString* uploadString = [[NSMutableAttributedString alloc] initWithString:upload];
	[uploadString addAttribute:NSForegroundColorAttributeName value:[NSColor grayColor] range:NSMakeRange(0,[uploadString length])];
	[uploadString addAttribute:NSFontAttributeName value:[NSFont menuBarFontOfSize:0] range:NSMakeRange(0,[uploadString length])];

	//Appel de la fonction de mise à jour sur le thread principal
	[self performSelectorOnMainThread:@selector(updateMenu:) withObject:[NSArray arrayWithObjects:downloadString,uploadString,downloadTotal,jours,nil] waitUntilDone:YES];
	
	//Libération de la mémoire
	[jours release];
	[info release];
	[downloadTotal release];
	[uploadString release];
	[downloadString release];
	[pool drain];
}
@end
