//
//  Controller.h
//
//  Created by Francis Valois on 11-02-25.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface Controller : NSObject {
    IBOutlet id affichageValeur;
}
- (IBAction)updateValue:(id)sender;
@end
