//
//  StatusMenuAppAppDelegate.h
//  StatusMenuApp
//
//  Created by Francis Valois on 11-02-25.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <AppKit/AppKit.h>

@interface StatusMenuAppAppDelegate : NSObject  {
    NSWindow *window;
    IBOutlet NSMenu *statusMenu;
    NSStatusItem * statusItem;
	IBOutlet id affichageJour;
	IBOutlet id affichageDownloadTotal;
	IBOutlet id affichageDownload;
	IBOutlet id affichageUpload;
}

- (IBAction)updateValue:(id)sender;
- (IBAction)quitApp:(id)sender;
- (void) updateValueCalc;
- (void) updateMenu:(NSArray*) Array;
- (void)timerUpdate:(NSTimer*)theTimer;
@property (assign) IBOutlet NSWindow *window;

@end
