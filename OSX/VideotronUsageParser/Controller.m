//
//  Controller.m
//
//  Created by Francis Valois on 11-02-25.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Controller.h"
#import "videotron.h"

@implementation Controller



- (IBAction)updateValue:(id)sender {
    videotron* info = [[videotron alloc] init];
	[info getInfo];
	NSString* utilisation = [NSString stringWithFormat:@"Utilisation Web : %.2f Go / %.2f Go", info.gbDone, info.gbMax];
	[affichageValeur setTitle:utilisation];
	[info dealloc];
}
@end
