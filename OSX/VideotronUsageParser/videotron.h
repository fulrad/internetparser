//
//  videotron.h
//  StatusMenuApp
//
//  Created by Francis Valois on 11-02-25.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface videotron : NSObject {
	NSString* connectionURL;
	NSString* infoURL;
	NSString* username;
	NSString* password;
	NSHTTPCookie* cookie;
	float gbDone;
	float gbMax;
	float gbUpload;
	float gbDownload;
	int joursRestants;
	
}
- (id) init;
- (int) getInfo;
- (void) connection;
- (NSString*) getValue;

@property (retain) NSString* connectionURL;
@property (retain) NSString* infoURL;
@property (retain) NSString* username;
@property (retain) NSString* password;
@property float gbDone;
@property float gbUpload;
@property float gbDownload;
@property float gbMax;
@property int joursRestants;
@end
